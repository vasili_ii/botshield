# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import requests
import base64

from django.views.generic import View, TemplateView
from django.http import (JsonResponse, HttpResponseBadRequest,
                         HttpResponseRedirect)


from .settings import RECAPTCHA_PUBLIC_KEY, RECAPTCHA_PRIVATE_KEY, REAL_IP
from .exceptions import (GoogleRecaptchaResponseError,
                         GoogleRecaptchaKeysError)


class BrowserCheckView(View):
    """
    Проверяет является ли текущий клиент браузером или это терминальная
    программа
    Соответствующую информацию пишет в сессию
    Вызывается исключительно в качестве ajax-запроса js-cкриптом,
    расположенным на каждой странице сайта
    Обязательно использовать csrf-защиту!
    """
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            mdict = dict()
            is_browser = request.session.get('is_browser')
            if not is_browser:
                current_ip = request.META["REMOTE_ADDR"]
                if REAL_IP in request.META:
                    current_ip = request.META[REAL_IP]
                user_ips_set = set(request.session.get('user_ips_set', ()))
                user_ips_set.add(current_ip)
                mdict = {'is_browser': True,
                         'user_ips_set': tuple(user_ips_set)}
                request.session.update(mdict)
                request.session.save()
            return JsonResponse({'ok': True})
        else:
            return HttpResponseBadRequest('only ajax requests requires')


class CheckGoogleRecaptchaView(View):
    def post(self, request, *args, **kwargs):
        """
        Принимаем токен от гуглрекапчи, обрабатываем его и если все норм -
        редиректим по указанному адресу
        """
        sess = requests.Session()
        url = 'https://www.google.com/recaptcha/api/siteverify'
        if not RECAPTCHA_PRIVATE_KEY:
            raise GoogleRecaptchaKeysError(
                'You have to specify BOTSHIELD_RECAPTCHA_PRIVATE_KEY')

        data = {'secret': RECAPTCHA_PRIVATE_KEY,
                'response': request.POST["g-recaptcha-response"]}
        resp = sess.post(url, data)
        if not resp.status_code == 200:
            raise GoogleRecaptchaResponseError(
                'Google response is not 200 code: {}'.format(resp.status_code))
        mdict = resp.json()
        request.session['recaptcha_success'] = mdict.get('success')
        request.session.save()
        url2redirect = request.POST['next']
        return HttpResponseRedirect(url2redirect)


class GoogleRecaptchaView(TemplateView):
    template_name = 'botshield/google_recaptcha.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        mnext = self.request.get_full_path()
        gnext = self.request.GET.get('next')
        if gnext:
            mnext = base64.b64decode(gnext)

        if not RECAPTCHA_PUBLIC_KEY:
            raise GoogleRecaptchaKeysError(
                'You have to specify BOTSHIELD_RECAPTCHA_PUBLIC_KEY')

        context['RECAPTCHA_PUBLIC_KEY'] = RECAPTCHA_PUBLIC_KEY
        context['next'] = mnext
        return context
