# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from .views import (BrowserCheckView, CheckGoogleRecaptchaView,
                    GoogleRecaptchaView)


app_name = 'botshield'


urlpatterns = [
    url('^browser-check/$', BrowserCheckView.as_view(),
        name='browser_check_url'),
    url('^check-recaptcha/$', CheckGoogleRecaptchaView.as_view(),
        name='check_google_recaptcha'),
    url('^show-recaptcha-4-ajax/$', GoogleRecaptchaView.as_view(),
        name='show_recaptcha_4_ajax'),
]
