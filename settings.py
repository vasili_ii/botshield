# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf import settings
import re


REDIRECT_TO = getattr(settings, "BOTSHIELD_REDIRECT_TO",
                      settings.LOGIN_REDIRECT_URL)


FROBOTS_TO_SKIP = (
    {'name': 'yandex',
     'robots_names': ('YandexBot', 'YandexAccessibilityBot',
                      'YandexDirectDyn', 'YandexImages', 'YandexVideo',
                      'YandexVideoParser', 'YandexMedia', 'YandexBlogs',
                      'YandexFavicons', 'YandexWebmaster',
                      'YandexPagechecker', 'YandexImageResizer',
                      'YandexAdNet', 'YandexDirect', 'YaDirectFetcher',
                      'YandexCalendar', 'YandexSitelinks', 'YandexMetrika',
                      'YandexNews', 'YandexNewslinks', 'YandexCatalog',
                      'YandexAntivirus', 'YandexMarket', 'YandexVertis',
                      'YandexForDomain', 'YandexSpravBot'),
     "host_suffixes": ('yandex.net.',  'yandex.net',
                       'yandex.com.', 'yandex.com',
                       'yandex.ru.', 'yandex.ru',)},

    {'name': 'google', 'robots_names': ('Googlebot', 'Mediapartners-Google',
                                        'AdsBot-Google', 'AdsBot-Google-Mobile',
                                        'FeedFetcher-Google',
                                        'Google-Read-Aloud'),
     "host_suffixes": ('googlebot.com', 'googlebot.com.',
                       'google.com', 'google.com.',
                       'google.ru', 'google.ru.',
                       'googlebot.ru', 'googlebot.ru.')},

    {'name': 'mail.ru', 'robots_names': ('Mail.RU_Bot',),
     "host_suffixes": ('mail.ru.', 'mail.ru')},

    {'name': 'yahoo.com', 'robots_names': ('Yahoo! Slurp',),
     "host_suffixes": ('yahoo.net', 'yahoo.net.',
                       'yahoo.com', 'yahoo.com.')},

)

ROBOTS_TO_SKIP = getattr(settings, 'BOTSHIELD_ROBOTS_TO_SKIP',
                         FROBOTS_TO_SKIP)


# time to cache ip-addresses-counters in redisdb
# время в течение которого считается количество посещений
TIME_TO_CACHE_IPS = getattr(settings, "BOTSHIELD_TIME_TO_CACHE_IPS",
                            60 * 60 * 3)

# max-count of visits per ip to redirect
# максимальное количество посещений ресурса в течение заданного промежутка
# времени для одного IP-адреса:
MAX_COUNT_PER_IP = getattr(settings, "BOTSHIELD_MAX_COUNT_PER_IP", 2)

# префикс для всех ключей бд редис, используемых приложением
REDIS_DB_KEYS_PREFIX = getattr(settings, "BOTSHIELD_REDIS_DB_KEYS_PREFIX",
                               'botshield')

REDIS_HOST = getattr(settings, "BOTSHIELD_REDIS_HOST", "localhost")
REDIS_PORT = getattr(settings, "BOTSHIELD_REDIS_PORT", "6379")
REDIS_DB = getattr(settings, "BOTSHIELD_REDIS_DB", "1")


# ip, которые исключаем из проверки
PRIVILIGED_IPS = getattr(settings, 'BOTSHIELD_PRIVILIGED_IPS',
                         ('localhost', '127.0.0.1'))


# время, в течение которого данные по ip-диапазонов роботов будут храниться
# в БД-редиса
ROBOT_KEY_EXPIRE_VALUE = getattr(settings, 'BOTSHIELD_ROBOT_KEY_EXPIRE_VALUE',
                                 60 * 60 * 24 * 100)


# работа с ботами (мидлваря, редиректы):
RE_BOT = getattr(settings, 'BOTSHIELD_RE_BOT',
                 re.compile('([a-zA-z_.]*bot)|([a-zA-z_.]*crawler)', re.I))
BOT_REDIRECT_TO = getattr(settings, 'BOTSHIELD_BOT_REDIRECT_TO',
                          '/for-bots.html')

# использовать в работе список ip-адресов прокси-серверов, что бы отдельно
# обрабатывать (быстрее отсекать) такие запросы
USE_PROXIES_LIST = getattr(
    settings, 'BOTSHIELD_USE_PROXIES_LIST', True)

RECAPTCHA_PUBLIC_KEY = getattr(
    settings, 'BOTSHIELD_RECAPTCHA_PUBLIC_KEY')
RECAPTCHA_PRIVATE_KEY = getattr(
    settings, 'BOTSHIELD_RECAPTCHA_PRIVATE_KEY')


REAL_IP = 'HTTP_X_REAL_IP'  # настройка заголовка реального ip от проксирующего сервера (nginx)

FUNC_TO_GET_PROXIES = getattr(settings, 'BOTSHIELD_FUNC_TO_GET_PROXIES', None)
