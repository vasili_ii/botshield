# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function

from .decorators import ipfilter
from .settings import (TIME_TO_CACHE_IPS, MAX_COUNT_PER_IP)


class IPFilterMixin(object):
    ipf_view_tag = ''
    ipf_period = TIME_TO_CACHE_IPS
    ipf_maxcount = MAX_COUNT_PER_IP

    @classmethod
    def as_view(cls, **initkwargs):
        view = super(IPFilterMixin, cls).as_view(**initkwargs)
        return ipfilter(cls.ipf_view_tag, cls.ipf_period,
                        cls.ipf_maxcount)(view)
