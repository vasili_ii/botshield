# -*- coding: utf-8 -*-

from __future__ import print_function, unicode_literals


from django.core.management.base import BaseCommand

from botshield.redis_models import ProxiesList


class Command(BaseCommand):

    def handle(self, *args, **options):
        print('Start recreate proxies list')
        ProxiesList().recreate()
        print('Done!')
