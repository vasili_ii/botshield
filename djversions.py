# -*- coding: utf-8 -*-


# module to work with different django versions

try:
    # django < 2
    from urllib2 import urlparse as url_parse
except Exception as err:
    # django >= 2
    import urllib.parse as url_parse

try:
    # django < 2
    from django.core.urlresolvers import reverse
except ImportError:
    # django >= 2
    from django.urls import reverse
