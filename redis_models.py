# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import itertools
import logging

from redis import Redis
from subprocess import Popen, PIPE

from django.utils import module_loading

from .settings import (REDIS_HOST, REDIS_PORT, REDIS_DB, REDIS_DB_KEYS_PREFIX,
                       TIME_TO_CACHE_IPS, MAX_COUNT_PER_IP,
                       ROBOT_KEY_EXPIRE_VALUE,
                       ROBOTS_TO_SKIP, FUNC_TO_GET_PROXIES)


redis = Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)


logger = logging.getLogger('botfield')


def check_count4ip(ip, view_tag, cache_time=None, mcount=None):
    """
    Проверяет ip на наличие в ДБ редис.
    Если отсутствует или присутствует, но не превышен лимит - возвращаем True,
    иначе - False
    """
    key = REDIS_DB_KEYS_PREFIX + ':counter_ip:' + view_tag + ':' + ip
    cache_time = cache_time or TIME_TO_CACHE_IPS
    mcount = mcount or MAX_COUNT_PER_IP
    count = int(redis.incr(key))
    res = False

    if count == 1:
        redis.expire(key, cache_time)
        res = True
    elif count <= mcount:
        res = True

    return res


def check_count4session(session_key, view_tag, cache_time=None, mcount=None):
    """
    Проверяет session_key на наличие в ДБ редис.
    Если отсутствует или присутствует, но не превышен лимит - возвращаем True,
    иначе - False
    """
    if not session_key:
        return True

    key = REDIS_DB_KEYS_PREFIX + ':counter_ses:' + view_tag + ':' + session_key
    cache_time = cache_time or TIME_TO_CACHE_IPS
    mcount = mcount or MAX_COUNT_PER_IP
    count = int(redis.incr(key))
    res = False

    if count == 1:
        redis.expire(key, cache_time)
        res = True
    elif count <= mcount:
        res = True

    return res


class Robot(object):
    def __init__(self, robot):
        self.name = robot['name']
        self.names_tuple = robot['robots_names']
        self.host_suffixes = robot['host_suffixes']
        self.key_pref = REDIS_DB_KEYS_PREFIX + ':robot:'
        self.key = self.key_pref + self.name

    def checkip(self, ip):
        """
        Returns True if ip in robot's ips set in redis-db,
        overwise returns False
        """
        return redis.sismember(self.key, ip)

    def addip(self, ip):
        """
        Adds ip into robot's set of ips in redis-db
        """
        keys = redis.keys((self.key_pref + '*'))
        if self.key not in keys:
            # если ключ создается впервые - задаем время жизни ключа
            redis.sadd(self.key, ip)
            redis.expire(self.key, ROBOT_KEY_EXPIRE_VALUE)
        else:
            redis.sadd(self.key, ip)

    def check_host(self, ip):
        cmd = 'host %s' % ip
        proc = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        out, err = proc.communicate()
        out = out.strip()
        if proc.returncode == 0:
            for suff in self.host_suffixes:
                suff = suff.encode('utf-8')
                if out.endswith(suff):
                    self.addip(ip)
                    return True
            msg = 'IP {0} is not in {1} hosts (out: "{2}")'.format(
                ip, self.name, out)
            logger.info(msg)
        return False

    def get_ips(self):
        """
        Returns all ips, registered for robot
        """
        return redis.smembers(self.key)

    @classmethod
    def ips_of_all_robots(cls):
        return set(itertools.chain(*(list(cls(robot).get_ips())
                                     for robot in ROBOTS_TO_SKIP)))


class ProxiesList(object):
    """
    Данный объект будет работать с множеством ip-адресов прокси-серверов
    Он используется для постоянного хранения множества ip-адресов прокси.
    К нему будет время от времени обращаться класс-синглтон для обновления
    внутреннего списка прокси.
    """

    key = REDIS_DB_KEYS_PREFIX + ':proxies_list'
    redis = redis

    def add(self, proxies_list):
        self.redis.sadd(self.key, *proxies_list)

    def clear(self):
        self.redis.delete(self.key)

    def recreate(self):
        proxies_list = self.get_proxies_list_from_outside()
        self.clear()
        self.add(proxies_list)

    def get_proxies_list_from_outside(self):
        mlist = []
        if FUNC_TO_GET_PROXIES:
            func2get_proxies = module_loading.import_string(FUNC_TO_GET_PROXIES)
            mlist = func2get_proxies()
        return mlist

    def get(self):
        return set(self.redis.smembers(self.key))

    @classmethod
    def check4proxy(cls, ipaddr):
        return cls.redis.sismember(cls.key, ipaddr)
