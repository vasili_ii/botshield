# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin

from .models import BotShieldSettings


class BotShieldSettingsAdmin(admin.ModelAdmin):
    list_display = ('description', 'name', 'value')
    list_editable = ('value',)
    search_fields = ('name',)
    readonly_fields = ('name', 'description')

    def get_queryset(self, request):
        qs = super(BotShieldSettingsAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs

admin.site.register(BotShieldSettings, BotShieldSettingsAdmin)
