# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function


class GoogleRecaptchaResponseError(Exception):
    pass


class GoogleRecaptchaKeysError(Exception):
    pass
