# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function

# import datetime

from django.core.urlresolvers import reverse

from django.test import TestCase
# from django.test.utils import override_settings
# from django.test.client import Client



class BotShieldTest(TestCase):

    def test_non_ajax_get_request(self):
        murl = reverse('botshield:browser_check_url')
        response = self.client.get(murl)
        self.assertEqual(response.status_code, 405)

    def test_non_ajax_post_request(self):
        murl = reverse('botshield:browser_check_url')
        response = self.client.post(murl)
        self.assertEqual(response.status_code, 400)

    def test_ajax_get_request(self):
        murl = reverse('botshield:browser_check_url')
        response = self.client.get(
            murl, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 405)

    def test_ajax_post_request(self):
        murl = reverse('botshield:browser_check_url')
        response = self.client.post(
            murl, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
