# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function


import logging
import base64

from functools import wraps
from django.http import JsonResponse


from .djversions import url_parse, reverse
from .settings import (PRIVILIGED_IPS,
                       ROBOTS_TO_SKIP,
                       USE_PROXIES_LIST, REAL_IP)
from .redis_models import (check_count4ip, check_count4session, Robot,
                           ProxiesList)
from .models import BotShieldSettings
from .views import GoogleRecaptchaView


logger = logging.getLogger('botfield')


def ipfilter(view_tag=None, period=None, maxcount=None):

    def ipfilter0(func):
        @wraps(func)
        def wrapper(request, *args, **kwargs):
            if request.session.get('recaptcha_success'):
                # если в сессии есть информация об удачно пройденной гуглокапче -
                # возвращаем заданную вьюху
                result = func(request, *args, **kwargs)
                return result

            ip = request.META.get('REMOTE_ADDR', '')
            ip = request.META.get(REAL_IP, ip)

            ua_name = request.META.get('HTTP_USER_AGENT', '')
            result = None

            if ip in PRIVILIGED_IPS or ip in Robot.ips_of_all_robots():
                # если ip в списке привелигированных - сразу возвращаем заданную
                # вьюху
                result = func(request, *args, **kwargs)
                return result

            check4robot, robot = check4robot_name(ua_name)
            if not check4robot:
                # если наименования робота в user-agent нет -
                # считаем запросы по сессии и ip и проверяем на возможность
                # вернуть запрашиваему вьюху
                botshield_settings = BotShieldSettings.get_settings_dict()
                maxcount = max(botshield_settings.values())
                request_from_proxy = False
                session_key = request.session.session_key

                if USE_PROXIES_LIST:
                    if ProxiesList.check4proxy(ip):
                        request_from_proxy = True
                client_is_browser = request.session.get('is_browser', False)

                if client_is_browser and request_from_proxy:
                    maxcount = botshield_settings['threshold_proxy_browser']
                elif not client_is_browser and request_from_proxy:
                    maxcount = botshield_settings['threshold_proxy_terminal']
                elif client_is_browser and not request_from_proxy:
                    maxcount = botshield_settings['threshold_browser']
                elif not client_is_browser and not request_from_proxy:
                    maxcount = botshield_settings['threshold_terminal']

                if check_count4ip(ip, view_tag, period, maxcount)\
                    and check_count4session(session_key, view_tag, period,
                                            maxcount):
                    result = func(request, *args, **kwargs)
            else:
                # если в наименовании user-agent-а присутствует один из
                # роботов - проверяем входит ли этот IP в зарегистрированный
                # для данного робота диапазон
                redis_robot = Robot(robot)
                if redis_robot.checkip(ip):
                    result = func(request, *args, **kwargs)
                else:
                    if redis_robot.check_host(ip):
                        result = func(request, *args, **kwargs)

            if not result:
                if request.is_ajax():
                    referer_url = request.META['HTTP_REFERER']
                    refurl = None
                    if referer_url:
                        urlparseobj = url_parse.urlparse(referer_url)
                        referer_url = urlparseobj.path
                        if urlparseobj.query:
                            referer_url += '?' + urlparseobj.query
                        refurl = base64.b64encode(referer_url)

                    show_grecaptcha_url = reverse(
                        'botshield:show_recaptcha_4_ajax')
                    if refurl:
                        show_grecaptcha_url += '?next={}'.format(refurl)
                    data = {"show_grecaptcha_url": show_grecaptcha_url}
                    return JsonResponse(data, status=403)

                result = GoogleRecaptchaView.as_view()(request)
                result.status_code = 403

            return result

        return wrapper
    return ipfilter0


def check4robot_name(ua_name):
    """
    Проверяет наименования роботов на вхождение в наименование http-клиента,
    пославшего запрос
    """
    for robot in ROBOTS_TO_SKIP:
        for rname in robot['robots_names']:
            if rname in ua_name:
                return True, robot

    return False, None
