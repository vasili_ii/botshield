# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import migrations


BTS_SETTINGS_LIST = [
    ('threshold_terminal', 'Порог для терминальных клиентов', 5),
    ('threshold_browser', 'Порог для браузерных клиентов', 200),
    ('threshold_proxy_terminal', 'Порог для терминальных клиентов c прокси', 3),
    ('threshold_proxy_browser', 'Порог для браузерных клиентов c прокси', 50),
]


def add_botshield_settings(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    BotShieldSettings = apps.get_model("botshield", "BotShieldSettings")
    models_list = []
    for name, desc, value in BTS_SETTINGS_LIST:
        models_list.append(BotShieldSettings(name=name, description=desc,
                                             value=value))
    BotShieldSettings.objects.bulk_create(models_list)


def empty_func(apps, schema_editor):
    BotShieldSettings = apps.get_model("botshield", "BotShieldSettings")
    BotShieldSettings.objects.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('botshield', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_botshield_settings, empty_func),
    ]
