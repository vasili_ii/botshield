# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function

from django import template

try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse

register = template.Library()


@register.inclusion_tag('botshield/js_block.html', takes_context=True)
def browser_checker(context, show_always=False):
    """
    show_always - используется для кэшируемых страниц. Для того, что бы всегда
    на кэшируемых страницах отображался скрипт с отправкой запроса. Таким образом,
    при работе с кэшируемыми страницами всегда ставим show_always=True

    Если работаем без кэша (всей страницы) - то используем без show_always=True.
    В таком случае смотрим на текущий реквест, его сессию и смотрим там наличие
    метки, что это браузер. Если is_browser is None (не отображался еще) - выводим
    скрипт, иначе (True/False) - признак уже сохранен и нет необходимости в добавлении
    скрипта в тело страницы.
    """
    is_browser = context.request.session.get('is_browser')
    browser_was_checked = True
    if is_browser is None:
        browser_was_checked = False
    return {
        'browser_check_url': reverse('botshield:browser_check_url'),
        'show_always': show_always,
        'browser_was_checked': browser_was_checked
    }
