# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function
import datetime

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.lru_cache import lru_cache


@python_2_unicode_compatible
class BotShieldSettings(models.Model):
    name = models.CharField(max_length=30, unique=True)
    description = models.CharField(max_length=255)
    value = models.PositiveIntegerField()

    def __str__(self):
        return '{0}: {1}'.format(self.description, self.value)

    class Meta:
        verbose_name = 'Бот-щит настройки'
        verbose_name_plural = 'Бот-щит настройки'

    @classmethod
    def get_settings_dict(cls):
        tm_tag = datetime.datetime.now().strftime('%Y-%m-%d-%H')
        return cls.cached_settings_dict(tm_tag)  # кэшируем в lru_cache (т.е. в рамках одного процесса на 1 час)

    @classmethod
    @lru_cache(maxsize=1)
    def cached_settings_dict(cls, tm_tag):
        mdict = {}
        for name, value in cls.objects\
                .all().values_list('name', 'value'):
            mdict[name] = value
        return mdict
